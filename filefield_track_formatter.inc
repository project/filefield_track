<?php
/**
 * Theme function for the 'default' filefield formatter.
 */
function theme_filefield_track_formatter_default_track($element) {
  $file = $element['#item'];

  // Views may call this function with a NULL value, return an empty string.
  if (empty($file['fid'])) {
    return '';
  }

  $url = filefield_track_path($file);
  $icon = theme('filefield_icon', $file);

  // Set options as per anchor format described at
  // http://microformats.org/wiki/file-format-examples
  $options = array(
    'attributes' => array(
      'type' => $file['filemime'] . '; length=' . $file['filesize'],
    ),
    'query' => array(
      'redirect' => $_GET['q'],
    ),
  );

  // Use the description as the link text if available.
  if (empty($file['data']['description'])) {
    $link_text = $file['filename'];
  }
  else {
    $link_text = $file['data']['description'];
    $options['attributes']['title'] = $file['filename'];
  }

  return '<div class="filefield-file">'. $icon . l($link_text, $url, $options) .'</div>';
}

/**
 * Theme function for the 'path_plain' formatter.
 */
function theme_filefield_track_formatter_path_plain_track($element) {
  // Inside a View this function may be called with null data. In that case,
  // just return.
  if (empty($element['#item'])) {
    return '';
  }

  $field = content_fields($element['#field_name']);
  $item = $element['#item'];
  // If there is no image on the database, use default.
  if (empty($item['fid']) && $field['use_default_file']) {
    $item = $field['default_file'];
  }
  if (empty($item['filepath']) && !empty($item['fid'])) {
    $item = array_merge($item, field_file_load($item['fid']));
  }
  return empty($item['filepath']) ? '' : check_plain(filefield_track_path($item));
}

/**
 * Theme function for the 'url_plain' formatter.
 */
function theme_filefield_track_formatter_url_plain_track($element) {
  // Inside a View this function may be called with null data. In that case,
  // just return.
  if (empty($element['#item'])) {
    return '';
  }

  $field = content_fields($element['#field_name']);
  $item = $element['#item'];
  // If there is no image on the database, use default.
  if (empty($item['fid']) && $field['use_default_file']) {
    $item = $field['default_file'];
  }
  if (empty($item['filepath']) && !empty($item['fid'])) {
    $item = array_merge($item, field_file_load($item['fid']));
  }

  if (empty($item['filepath'])) {
    return '';
  }

  return $GLOBALS['base_url'] . '/' . filefield_track_path($item);
}

/**
 * Builds the trackable path to a file.
 * @param $file
 *   indexed array of file information
 * @param $redirect
 *   boolean to determine if a redirect should be appended to the path
 */
function filefield_track_path($file) {
  return drupal_urlencode('download/' . $file['fid'] . '/' . $file['filename']);
}
