<?php
/**
 * Gatekeeper form that stands between the files. 
 */
function filefield_track_download_file_form($form_state, $fid) {
  $form = array();

  $step = isset($form_state['storage']['step']) ? $form_state['storage']['step'] + 1 : 1;

  global $user;

  // Non-anonymous users use account email.
  if ($user->uid) {
    filefield_track_log($fid, $user->mail, $user->uid); // Trying to insert user's email
    filefield_track_get_file($fid);
  }
  elseif (!empty($_COOKIE['fft_auth'])) {
    list($email, $auth) = explode("|", $_COOKIE['fft_auth']);
    $result = db_query("SELECT * FROM {filefield_track} WHERE email = '%s' AND auth = '%s'", $email, $auth);
    $auth_obj = db_fetch_object($result);
    if ($auth_obj != NULL && $auth_obj->email == $email) {
      filefield_track_log($fid, $email);
      filefield_track_get_file($fid);
    }
  }

  $form['download']['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Please provide a valid email address to download this file'),
  );
  $form['download']['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  $form['download']['fid'] = array(
    '#type' => 'value',
    '#value' => $fid,
  );
  $form['#redirect'] = $_GET['redirect'];

  return $form;
}

function filefield_track_download_file_form_validate($form, &$form_state) {
  if (!valid_email_address($form_state['values']['email'])) {
    form_set_error('download', t('Please enter a valid email address.'));
  }
}

function filefield_track_download_file_form_submit($form, &$form_state) {
  $fid = $form_state['values']['fid'];
  $email = $form_state['values']['email'];
  
  // Just in case email has been used before but cookie was deleted. 
  $record = db_fetch_array(db_query("SELECT * FROM {filefield_track} WHERE email='%s'", $email));

  if (empty($record)) {
    $record = array(
      'fid' => $fid,
      'email' => $email,
      'auth' => md5(uniqid("")),
      'changed' => date('Y-m-d H:i'),
    );
    drupal_write_record('filefield_track', $record);
  }
  setcookie("fft_auth", $email ."|". $record['auth'], strtotime('+10 years'));

  filefield_track_log($fid, $email);
  filefield_track_get_file($fid);
}


function filefield_track_log($fid, $email, $uid = 0) {
  $result = db_query('SELECT * FROM {filefield_track} WHERE ((uid = 0 AND email = "%s") OR (uid != 0 AND uid = %d)) AND fid = %d', $email, $uid, $fid);

  if ($record = db_fetch_array($result)) {
    $update = 'did';
  }
  else {
    $record = array(
      'uid' => $uid,
      'fid' => $fid,
      'email' => $email,
    );
  }
  $record['count']++;
  $record['changed'] = date('Y-m-d H:i');
  drupal_write_record('filefield_track', $record, $update);
}

function filefield_track_get_file($fid) {
  $file = db_fetch_array(db_query('SELECT * FROM {files} WHERE fid = %d', $fid));
  // Make sure the file exists
  if (!file_exists($file['filepath'])) {
    drupal_not_found();
    exit();
  }

  header("Cache-Control: public, must-revalidate");
  header("Pragma: hack");
  header("Content-Type: " . $file['filemime']);
  header("Content-Length: " . (string)(filesize($file['filepath'])));
  header('Content-Disposition: attachment; filename="' . $file['filename'] . '"');
  header("Content-Transfer-Encoding: binary\n");

  readfile($file['filepath']);
  exit();
}
